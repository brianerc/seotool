<?php
require('FuncionesBD.php');
$yaExisteUsuario = '';
$yaExisteCorreo = '';
$mailInvalido = '';
$contraseniaInsegura = '';
$passwordNoCohincide = '';
$error='';
$exito='';
if(isset($_POST['submit'])) {
    if(!empty($_POST['email']) && isset($_POST['email']) &&  !empty($_POST['password']) &&  isset($_POST['password']) 
        &&  !empty($_POST['confirmar-password']) &&  isset($_POST['confirmar-password'])
        &&  !empty($_POST['nickname']) &&  isset($_POST['nickname'])) {
        $email=$_POST['email'];
        $password=$_POST['password'];
        $repassword=$_POST['confirmar-password'];
        $nickname=$_POST['nickname'];
        $plan=1;
        $funciones=new FuncionesBD();
        $hayError=false;
        if($funciones->existeCorreo($email)) {
            $yaExisteCorreo= "<span>Ya existe un usuario con ese correo</span>";
            $hayError=true;
        }
        if($funciones->existeUsuario($nickname)) {
            $yaExisteUsuario="<span>Ya existe un usuario con ese nombre</span>";
            $hayError=true;
        }
        if($password!=$repassword) {
            $passwordNoCohincide="<span>Las passwords no cohinciden.</span>";
            $hayError=true;
        }
        if (!filter_var($email,FILTER_VALIDATE_EMAIL)) {
            $mailInvalido="<span>El correo introducido es incorrecto.</span>";
            $hayError=true;
        }
        if(strlen($password)<8) {
            $contraseniaInsegura="<span>La password debe contener al menos 8 caracteres.</span>";
            $hayError=true;
        }
        if(!$hayError) {
            $registro=$funciones->registrarUsuario($nickname,$email,$password,$plan);
            $exito='<span>Se ha registrado correctamente, confirme su email para activar su cuenta.</span>';
        } 
        
    } else {
        if(empty($_POST['nickname'])) {
            $error="<span>Debe introducir un nickname</span>";
        }
        if(empty($_POST['email'])) {
            $error=$error."<span>Debe introducir un email</span>";
        }
        if(empty($_POST['password'])) {
            $error=$error."<span>Debe introducir un password</span>";
        }
        if(empty($_POST['confirmar-password'])) {
            $error=$error."<span>Debe confirmar el password</span>";
        }
        
    }
}
?>
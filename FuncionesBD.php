<?php
include 'iFuncionesBD.php';
class FuncionesBD implements iFuncionesBD {
    private function conectar() {
        $usuario="root";
        $contraseña="";
        $servidor="localhost";
        $bd="seotool";
        $conexion=mysqli_connect($servidor,$usuario,$contraseña,$bd);
        echo mysqli_error($conexion);
        return $conexion;
    }
    private function desconectar($conexion) {
        mysqli_close($conexion);
    }
    public function existeUsuario($nombre) {
        $conexion=$this->conectar();
        $consulta="Select count(nombre) as cantidad from usuario where nombre='$nombre'";
        $consulta=mysqli_query($conexion, $consulta);
        $consulta=mysqli_fetch_array($consulta);
        $consulta=$consulta["cantidad"];
        $this->desconectar($conexion);
        if($consulta>0) {
            return true;
        } else {
        return false;
        }
    }
    public function existeCorreo($correo) {
        $conexion=$this->conectar();
        $consulta="Select count(correo) as cantidad from usuario where correo='$correo'";
        $consulta=mysqli_query($conexion, $consulta);
        $consulta=mysqli_fetch_array($consulta);
        $consulta=$consulta["cantidad"];
        $this->desconectar($conexion);
        if($consulta>0) {
            return true;
        } else {
            return false;
        }
    }
    public function registrarUsuario($nombre,$correo,$contraseña,$plan) {
        //aca tengo que poner el existe.
        if($this->existeUsuario($nombre)==true) {
            return false;
        }
        if($this->existeCorreo($correo)==true) {
            return false;
        }
        $conexion=$this->conectar();
        $consulta="insert into usuario(nombre,correo,contrasena,plan_id) values('$nombre','$correo',AES_ENCRYPT('$contraseña','b3rc0'),'$plan')";
        mysqli_query($conexion, $consulta);
        $this->desconectar($conexion);
        return true;
    }
    public function iniciarSesion($nombre,$password) {
        $conexion=$this->conectar();
        $consulta="Select count(nombre) as nombre from usuario where nombre='$nombre' and contrasena=AES_ENCRYPT('$password','b3rc0')";
        $consulta=mysqli_query($conexion, $consulta);
        $consulta=mysqli_fetch_array($consulta);
        $consulta=$consulta["nombre"];
        $this->desconectar($conexion);
        if($consulta>0){
            return true;
        } else {
            return false;
        }
    }
    
    public function activarUsuario(){ }
    public function cambiarContraseña($nuevaContraseña){ }
    public function editarUsuario($nuevoNombre){ }
    public function cambiarPlan($nuevoPlan){ }
    public function crearCategoria($nombre,$color){ }
    public function eliminarCategoria($nombre){ }
    public function listaCategorias(){ }
    public function cambiarColorCategoria($nuevoColor){ }
    public function crearProyecto($nombre,$categoria,$url,$buscador){ }
    public function editarProyecto($nombre,$categoria,$url,$buscador){ }
    public function eliminarProyecto($nombre){ }
    public function listaProyectos(){ }
    public function agregarKeyword($nombre){ }
    public function eliminarKeyword($nombre){ }
    public function agregarBackLink($url){ }
    public function eliminarBackLink($url){ }
    public function listaBackLinks(){ } }

?>
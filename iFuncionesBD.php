<?php
interface iFuncionesBD {
    public function registrarUsuario($nombre,$correo,$contraseña,$plan);
    public function existeUsuario($nombre);
    public function existeCorreo($correo);
    public function iniciarSesion($nombre,$password);
    public function activarUsuario();
    public function cambiarContraseña($nuevaContraseña);
    public function editarUsuario($nuevoNombre);
    public function cambiarPlan($nuevoPlan);
    public function crearCategoria($nombre,$color);
    public function eliminarCategoria($nombre);
    public function listaCategorias();
    public function cambiarColorCategoria($nuevoColor);
    public function crearProyecto($nombre,$categoria,$url,$buscador);
    public function editarProyecto($nombre,$categoria,$url,$buscador);
    public function eliminarProyecto($nombre);
    public function listaProyectos();
    public function agregarKeyword($nombre);
    public function eliminarKeyword($nombre);
    public function agregarBackLink($url);
    public function eliminarBackLink($url);
    public function listaBackLinks();
}
?>